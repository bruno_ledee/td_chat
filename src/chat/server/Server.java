package chat.server;

import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;

import chat.components.Connection;
import chat.components.ConnectionHelper;
import chat.components.Emitter;
import chat.components.EmitterHelper;
import chat.components.Receiver;

public class Server {
	
	private Map<String, Receiver> clientsMap = new Hashtable<String, Receiver>();
	private static Server server;
	private static ORB orb = null;
	private Server() {}
	
	public static Server getServer() {
		if(server==null) server = new Server();
		return server;
	}

	public static void main(String args[]) {

		java.util.Properties props = System.getProperties();

		int status = 0;

		try {

			orb = ORB.init(args, props);
			run(orb);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = 1;
		}

		if (orb != null) {
			try {
				orb.destroy();
			} catch (Exception ex) {
				ex.printStackTrace();
				status = 1;
			}
		}

		System.exit(status);
	}

	static int run(ORB orb) throws Exception {
		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));

		org.omg.PortableServer.POAManager manager = rootPOA.the_POAManager();

		Connection_impl connectionImpl = new Connection_impl();
		obj = rootPOA.servant_to_reference(connectionImpl);
		Connection connection = ConnectionHelper.narrow(obj);

		obj = orb.resolve_initial_references("NameService");

		NamingContext ctx = NamingContextHelper.narrow(obj);

		if (ctx == null) {
			System.out.println(new Date() + " : " + "Le composant NameService n'est pas un repertoire");
			return 0;
		}

		NameComponent[] name = new NameComponent[1];

		name[0] = new NameComponent("Connection", "");

		ctx.rebind(name, connection);

		System.out.println(new Date() + " : " + "Objet cr�� et r�f�renc�");

		manager.activate();
		orb.run();

		return 0;
	}
	
	public Emitter addClient(String pseudo, Receiver rcv) throws Exception {
		
		String nicknameGiven = findAUniqueNickname(pseudo);
		
		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));
		
		Emitter_impl emitterImpl = new Emitter_impl(nicknameGiven);
		obj = rootPOA.servant_to_reference(emitterImpl);
		Emitter emitter = EmitterHelper.narrow(obj);
		
		clientsMap.put(nicknameGiven, rcv);
		rcv.initClients(getClientsList());
		for(Receiver oneRcv:getAllReceivers()) oneRcv.addClient(nicknameGiven);
		System.out.println(new Date() + " : " + nicknameGiven + " connect�(e)");
		return emitter;
	}
	
	public void removeClient(Emitter emt) {
		String nickName = emt.getMyNickname();
		clientsMap.remove(nickName);
		for(Receiver oneRcv:getAllReceivers()) oneRcv.remClient(nickName);
		System.out.println(new Date() + " : " + nickName + " d�connect�(e)");
	}
	
	public String[] getClientsList() {
		return (String[]) clientsMap.keySet().toArray(new String[0]);
	}
	
	public void sendMessage(String from, String to, String text) {
		Receiver rcv = clientsMap.get(to);
		if(rcv!=null) rcv.receive(from, text);
	}
	
	private String findAUniqueNickname(String nickname) {
		if(!clientsMap.containsKey(nickname)) return nickname;
		else {
			int i = 0;
			String newNickname;
			do {
				i++;
				newNickname = nickname + i;
			}
			while(clientsMap.containsKey(newNickname));
			return newNickname;
		}
	}
	
	private Receiver[] getAllReceivers() {
		Receiver[] rcvs = new Receiver[clientsMap.size()];
		int i = 0;
		for(Receiver rcv:clientsMap.values()){
			rcvs[i] = rcv;
			i++;
		}
		return rcvs;
	}
}
