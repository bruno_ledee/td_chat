package chat.server;

import chat.components.ConnectionPOA;
import chat.components.Emitter;
import chat.components.Receiver;

public class Connection_impl extends ConnectionPOA {

	@Override
	public Emitter connect(String nickname, Receiver rcv) {
		try {
			return Server.getServer().addClient(nickname, rcv);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void disconnect(Emitter connection) {
		Server.getServer().removeClient(connection);
		
	}

}
