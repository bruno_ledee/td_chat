package chat.server;

import java.util.Date;

import chat.components.EmitterPOA;

public class Emitter_impl extends EmitterPOA {
	
	private String myNickname;

	public Emitter_impl(String nicknameGiven) {
		super();
		this.myNickname = nicknameGiven;
	}
	
	@Override
	public void sendMessage(String to, String message) {
		Server.getServer().sendMessage(myNickname, to, message);
		System.out.println(new Date() + " : " + myNickname + " envoie un message a " + to);
	}

	@Override
	public String getMyNickname() {
		return myNickname;
	}

}
