package chat.common;

import org.omg.CORBA.ORB;

public class ORBRunner extends Thread {

	ORB orb;
	
	public ORBRunner(ORB orb) {
		this.orb = orb;
	}

	@Override
	public void run() {
		orb.run();
		
	}
}
