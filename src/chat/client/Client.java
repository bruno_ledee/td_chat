package chat.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;

import chat.common.ORBRunner;
import chat.components.Connection;
import chat.components.ConnectionHelper;
import chat.components.Emitter;
import chat.components.Receiver;
import chat.components.ReceiverHelper;

import org.omg.CORBA.ORBPackage.InvalidName;

public class Client {

	private static ORB orb = null;
	public static Connection connection = null;
	
	private Client(){
		
	}

	public static void main(String[] args) {

		java.util.Properties props = System.getProperties();

		int status = 0;

		try {
			orb = ORB.init(args, props);
			run(orb);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = 1;
		}

		if (orb != null) {
			try {
				orb.destroy();
			} catch (Exception ex) {
				ex.printStackTrace();
				status = 1;
			}
		}

		System.exit(status);
	}

	static void run(ORB orb) throws Exception {
		org.omg.CORBA.Object obj = null;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));
		org.omg.PortableServer.POAManager manager = rootPOA.the_POAManager();
		manager.activate();

		try {

			obj = orb.resolve_initial_references("NameService");
		} catch (InvalidName e) {
			e.printStackTrace();
			System.exit(1);
		}

		NamingContext ctx = NamingContextHelper.narrow(obj);

		if (ctx == null) {
			System.out.println("Le composant NameService n'est pas un repertoire");
			System.exit(1);
		}

		NameComponent[] name = new NameComponent[1];

		name[0] = new NameComponent("Connection", "");

		try {
			obj = ctx.resolve(name);
		} catch (Exception e) {
			System.out.println("Composant inconnu");
			e.printStackTrace();
			System.exit(1);
		}

		connection = ConnectionHelper.narrow(obj);

		Thread t = new ORBRunner(orb);
		t.start();

		//		scenarioTest();
		startGui();

	}

	public static Receiver createReceiver() throws Exception {

		org.omg.CORBA.Object obj;
		org.omg.PortableServer.POA rootPOA = org.omg.PortableServer.POAHelper
				.narrow(orb.resolve_initial_references("RootPOA"));

		Receiver_impl receiverImpl = new Receiver_impl();
		obj = rootPOA.servant_to_reference(receiverImpl);
		Receiver receiver = ReceiverHelper.narrow(obj);

		System.out.println(new Date() + " : Nouveau Receiver cr��");

		return receiver;
	}

	public static void scenarioTest() {

		// Procedure de test push

		try {

			Receiver rcv1 = createReceiver();
			Emitter emt1 = connection.connect("Peng", rcv1);

			Receiver rcv2 = createReceiver();
			Emitter emt2 = connection.connect("Bruno", rcv2);

			emt1.sendMessage("Bruno", "Salut, �a va ?");
			emt2.sendMessage("Peng", "Oui et toi ?");
			emt1.sendMessage("Bruno", "D�sol� je dois y aller.");

			connection.disconnect(emt1);
			connection.disconnect(emt2);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void startGui() throws Exception {

		boolean exit = false;
		/** 
		 * Diff�rents statues :
		 * 0 : d�connect� ; 1 : connect� entrain de choisir un destinataire ; 
		 * 2 : connect� entrain d'�crire un message
		 * 
		 */
		int statue = 0;
		String pseudo = "default";
		Receiver rcv = null; 
		Emitter emt = null;
		String to = "";

		String help = "arreter le chat [exit] ; se connecter [connect + pseudo] ; se deconnecter [disconnect]";
		String messageAccueil = "Bienvenue sur ce systeme de chat � base de composants distribu�s !\n";
		String messageDeconnecte = "Connectez-vous pour utiliser le chat.\n";
		String messageConnecte = "Vous �tes connect�(e) sous le nom de : ";
		String messageChoisirUnDestinataire = "Tapez le nom de la personne � qui vous voulez parler.\n";
		String messageEnvoyerMessage = "Tapez votre message pour : ";
		String messageMessageEnvoye = "Votre message a �t� envoy�.\n";
		String messageErreurDeconnection = "Vous n'�tes pas connect�.\n";
		String messageErreurCommandeNonReconnue = "La commande ou le nom du destinataire que vous avez �crit n'a pas �t� reconnu.\n";
		String message = messageAccueil + messageDeconnecte + help;

		BufferedReader userIn = new BufferedReader(new InputStreamReader(System.in));

		// Basic chat system
		while (!exit) {
			System.out.println(message);
			while (userIn.ready());
			String requete = userIn.readLine();
			String[] args = requete.split(" ");

			if("exit".equals(requete)) {
				if(statue != 0) 
					connection.disconnect(emt);
				exit = true;
			}

			else if("connect".equalsIgnoreCase(args[0]) && statue == 0) {

				if(args.length > 1) {
					pseudo = "";
					for(int i = 1; i<args.length; i++) 
						pseudo = pseudo + args[i];
				}
				else pseudo = "anonymous";

				statue = 1;

				message = messageChoisirUnDestinataire + messageConnecte + pseudo + "\n" + help;

				rcv = createReceiver();
				emt = connection.connect(pseudo, rcv);

			}

			else if(!"connect".equalsIgnoreCase(args[0]) && statue == 0) {
				message = messageErreurCommandeNonReconnue + messageDeconnecte + help;
			}

			else if("disconnect".equals(requete) && statue == 0) {
				message = messageErreurDeconnection + messageDeconnecte + help;
			}

			else if("disconnect".equals(requete) && statue != 0) {
				connection.disconnect(emt);
				statue = 0;
				message = messageAccueil + messageDeconnecte + help;
			}

			else if(statue == 1) {

				boolean valide = false;

				for(String s:rcv.getClientsList()) {
					if(s.equals(requete)) {
						to = requete;
						valide = true;
						break;
					}
				}

				if(valide){
					statue = 2;
					message = messageEnvoyerMessage + to + "\n" + messageConnecte + pseudo + "\n" + help;
				}
				else {
					message = messageErreurCommandeNonReconnue + messageConnecte + pseudo + "\n" + help;
				}
			}

			else if(statue == 2) {
				emt.sendMessage(to, requete);
				message = messageMessageEnvoye + messageChoisirUnDestinataire + messageConnecte + pseudo + "\n" + help;
			}
		}
	}

}
