package chat.client;

import java.util.ArrayList;
import java.util.List;

import chat.components.ReceiverPOA;

public class Receiver_impl extends ReceiverPOA {

	public List<String> listClients = new ArrayList<String>();
	
	@Override
	public void receive(String from, String message) {
		System.out.println(from + " : " + message);
	}

	@Override
	public void initClients(String[] clients) {
		listClients = new ArrayList<String>();
		System.out.println(this + " : Initialisation de la liste des clients");
		
		if(clients==null)return;
		for(String s:clients) listClients.add(s);
		
		for(String s:clients) {
			System.out.println(this + " : " + s + " est connect�(e)");
		}
	}

	@Override
	public void addClient(String client) {
		listClients.add(client);
		System.out.println(this + " : " + client + " se connecte");
	}

	@Override
	public void remClient(String client) {
		listClients.remove(client);
		System.out.println(this + " : " + client + " se d�connecte");
		
	}

	@Override
	public String[] getClientsList() {
		return (String[]) listClients.toArray(new String[0]);
	}

}
